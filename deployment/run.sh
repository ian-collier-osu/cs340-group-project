#!/bin/bash

build() {
	rootDir="$(git rev-parse --show-toplevel)"
	pushd "$rootDir" > /dev/null

	docker image build . --tag "cs340-proj" \
						 --file "$rootDir"/deployment/Dockerfile

	popd

}

run() {
	docker run -p 8000:8000 -it cs340-proj:latest
}

run-ci() {
	set -e
	# Restart
	docker ps -q --filter ancestor="cs340-proj:latest" | xargs -r docker stop
	run
}

deploy() {
	set -e
	build
	run
}

case $1 in
  build|run|deploy) "$1" ;;
  *) echo "Usage: ./run.sh [build/run/deploy/run-ci]" ;;
esac
